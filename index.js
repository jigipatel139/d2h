const readline = require('readline')
const TarrifPackage = require('./service/tarrifPackage')

const packages = new TarrifPackage()

const readlineInterface = readline.createInterface({
    input:process.stdin,
    output: process.stdout,
})

// List of questions
const questions = `Welcome. What would you like to do? Please choose, \n
1. To view your balance \n
2. To add amount to your balance \n
3. To view your basic tariff package \n
4. To add addon channel to your tariff package \n
5. To remove the channel from your tariff plan \n`

// Function to render the main menu
const commandLineMenu = ()=>{
    readlineInterface.question(questions,(userInput)=>{
        userInput = parseInt(userInput)
        switch (userInput) {
            case 1:
                // To view user current balance
                const balance = packages.getBalance()
                console.log("your balance is "+balance)
                returnToMainMenu()
                break
            case 2:
                // Add amunt to user balance
                addAmount()
                break
            case 3:
                // To view channel package
                const channels = packages.gethannels()
                console.log(channels)
                returnToMainMenu()
                break
            case 4:
                // Add channel to user package
                addon()
                break
            case 5:
                // Remove channel from package
                removeChannel()
                console.log("in case 5")
                break
            default:
                console.log("Invalid choise")
                returnToMainMenu()
        }
    })
}

// Function to return to the main menu
const returnToMainMenu = ()=>{
    readlineInterface.question("Press enter to return to the main menu\n",(userInput)=>{
            commandLineMenu()
    })
}

// Function to accept amount from user
const addAmount = ()=>{
    readlineInterface.question("Please enter the amount\n",(amount)=>{
        amount = parseInt(amount)
        packages.addBalance(amount)
        returnToMainMenu()
    })
}

// Function to ask user for channel addition
const addon = ()=> {
    console.log("Choose any one categories of channel:")
    let cat = packages.getCategories()
    readlineInterface.question(`${cat}`, (categoryIndex) => {
        let channels = packages.getChannelsByCategory(categoryIndex)
        readlineInterface.question(`Enter channel from belove list\n${channels} \n`,(channel)=>{
            const res = packages.addChannel(channel)
            console.log(res)
            returnToMainMenu()
        })
    })
}

// Function to accept from user for remove it
const removeChannel = ()=>{
    readlineInterface.question('Enter channel to remove\n',(channel)=>{
        const res = packages.removeChannel(channel)
        console.log(res)
        returnToMainMenu()
    })
}

// Call main function
commandLineMenu()
