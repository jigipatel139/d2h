const TarrifPackage = require('../service/tarrifPackage')

const packages = new TarrifPackage()

describe("Tarrif Package test",()=>{
    test("Initial package",()=>{
        const channels = packages.gethannels()
        expect(channels).toEqual([ 'StarPlus', 'StarGold', 'SonyTV' ])
    })
    test("Add channel to package without balance",()=>{
        const channel = "Animal Planet"
        const res = packages.addChannel(channel)
        expect(res).toBe(`You don’t have sufficient balance`)
    })
    test("Add channel to package",()=>{
        const channel = "Animal Planet"
        packages.addBalance(100)
        const res = packages.addChannel(channel)
        expect(res).toBe(`${channel} channel added successfully`)
    })
    test("Remove channel",()=>{
        const channel = "Animal Planet"
        const res = packages.removeChannel(channel)
        expect(res).toBe(`${channel} channel removed`)
    })
    test("View channel after adding",()=>{
        const channel = "Zee24"
        const res = packages.addChannel(channel)
        const channels = packages.gethannels()
        expect(channels).toEqual([ 'StarPlus', 'StarGold', 'SonyTV', 'Zee24' ])
    })
    test("View categories",()=>{
        let cat = packages.getCategories()
        expect(cat).toBe('1. Entertainment \n2. Educational \n3. Regional \n4. Sports \n')
    })
    test("View channels by categories",()=>{
        let channels = packages.getChannelsByCategory(1)
        expect(channels).toEqual(["ZeeTV", "Colors", "&pictures", "Box Cinema"])
    })
})

