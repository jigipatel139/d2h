const TarrifPackage = require('../service/tarrifPackage')

const packages = new TarrifPackage()

describe("Account test",()=>{
    test("Initial balance should be 0",()=>{
        const balance = packages.getBalance()
        expect(balance).toBe(0)
    })
    test("Adding amount",()=>{
        packages.addBalance(100)
        const balance = packages.getBalance()
        expect(balance).toBe(100)
    })
})

