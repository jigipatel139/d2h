const Account = require('./account')
const readline = require('readline')

const readlineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})


class TarrifPackage extends Account {
    channels = ["StarPlus", "StarGold", "SonyTV"]
    channelPrice = 10
    channelCategory = {
        "Entertainment": ["ZeeTV", "Colors", "&pictures", "Box Cinema"],
        "Educational": ["Animal Planet", "Discovery", "Safari TV", "National Geographic"],
        "Regional": ["Aaj Tak", "CNBC", "ETV", "Zee24"],
        "Sports": ["DD Sports", "Star Sports", "Sony TEN"],
    }

    constructor() {
        super()
    }

    // Decription: View channels
    // Return: array - channels
    gethannels() {
        return this.channels
    }

    // Description: Add channel to package
    // Param: string - channel name
    // Return: string - Error or Success Message
    addChannel(channel) {
        if (this.channels.includes(channel)) {
            return "Channel is already exists"
        } else {
            let balance = this.balance
            if (balance < this.channelPrice) {
                return 'You don’t have sufficient balance'
            } else {
                this.channels.push(channel)
                this.balance = this.balance - this.channelPrice
                return `${channel} channel added successfully`
            }
        }
    }

    // Description: Remove channel from package
    // Param: string - Channel name
    // Return: String - Message 
    removeChannel(channel) {
        this.channels.splice(this.channels.indexOf(channel), 1)
        return `${channel} channel removed`
    }

    // Description: Get list of categories
    // Return: string - List of categories with number
    getCategories() {
        const categories = Object.keys(this.channelCategory)
        let catList = ""
        for (let i = 0; i < categories.length; i++) {
            catList += `${i + 1}. ${categories[i]} \n`
        }
        return catList
    }

    // Description: Get channels based on category
    // Param: int - index of the category
    // Return: string - list of channels
    getChannelsByCategory(categoryIndex) {
        const categories = Object.keys(this.channelCategory)
        const channels = categories[categoryIndex - 1]
        return this.channelCategory[channels]
    }
}

module.exports = TarrifPackage