
class Account {
    balance = 0
    constructor() {
        this.balance = 0
      }

      // Description: View balance
      // Return: int - balance
      getBalance() {
        return this.balance
      }

      // Description: Add amount to the current balance
      // Param: int - amount
      addBalance(amount){
          this.balance = this.balance + amount
      }
}

module.exports = Account